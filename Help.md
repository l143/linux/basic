# Comandos de ayuda

Estos comandos proporcionan una ayuda sobre los demás comandos/utilidades que tenemos instalados en el equipo.

> **man** 

*Man nos permite ver los manuales que traen consigo algunos paquetes/utilidades*

```bash
$ man [command]
```


> **apropos** 

*Los **man**uales contienen una descripción, apropos nos ayuda a leer una descripción rápida del comando.*

```bash
$ aprops [command]
```

> [tldr](https://tldr.sh/)  

**Too long dont read**, es un paquete que nos puede dar ejemplos de comandos para complementar los manuales o evitar leer demasiado.

```bash
$ sudo apt-get install tldr # Instalación
$ tldr -u # Actualizamos las entradas del comando
$ tldr [command] # Muestra ejemplos de uso en un comando
```