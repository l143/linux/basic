# Componentes y Arquitectura

**Tabla de contenidos**

  - [Componentes](#Componentes)
  - [Arquitectura](#Arquitectura)


---

<br>

## Componentes
|Nombre|Descripción|
|--|--|
|bootloader|Se encarga de guiar el proceso de encendido e inicio del sistema operativo.|
|Os Kernel|Principal componete del sistema operativo, maneja los recursos para entradas y salidas de los dispositivos a nivel de hardware.|
|Daemons|Son programas que iniciados al encender el equipo o iniciar sesión, se encargan de vigilar que algunas funciones trabajen correctamente.|
|OS Shell|Es una interface entre el sistema operativo y el usuario. Permite instruir al sistema, algunos ejemplos son bash, zsh, fish|
|Graphics server|Permite que subsistemas gráficos puedan correr local o remotamente, se componen de **x-server** y **x-windowing**|
|Window Manager|También son conocidos como GUI (Graphical User Interface) y permiten entre otras cosas, navegar en la web y archivos. Algunos ejemplos son GNOME, KDE, MATE, Unity o Cinnamon.|
|Utilidades| Applicaciones y utilidades extras para realizar tareas adiconales.|

<br>

## Arquitectura
|Nombre|Descripción|
|--|--|
|Hardware| Componente periféricos como la RAM, disco duro, CPU, etc... |
|Kernel|Es el core del sistema que administra los recursos físicos del mismo.|
|Shell|Interface de línea de comandos|
|Utilidades de sistema|Pone al alcance del usuario todas las capacidades del sistema.|