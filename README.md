# Linux


## Kernel

Se le conoce como linux, al Kernel o core de un sistema operativo que se usa para tener el control sobre los recursos de un equipo de cómputo (almacenamiento, memoria volátil, tarjetas de red, etc...).

- [Versiones de kernel](https://www.kernel.org/)

## Distribución

El concepto de distribución se refiere a el kernel aunado a una serie de herramientas que permiten las operaciones con archivos, manejo de usuarios y manejadores de paquetes.

Ejemplos de distribuciones:
 
 - RHEL
 - CentOS (desaparecerá a fines del 2021)
 - openSUSE
 - SLES
 - Ubuntu

## Indice
- [Componentes del sistema](./Components.md)
- [Ayuda](./Help.md)
- [Manejo de recursos](./Resources.md)
- [Usuarios, grupos y permisos](./Users_Groups_Permissions.md)
