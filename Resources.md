# Recursos del sistema

**Tabla de contenidos**

- [🖥 CPU](#🖥-CPU)
- [🧮 Memoria](#🧮-Memoria)
- [💽 Almacenamiento](#💽-Almacenamiento)
- [🧭 Zona horaria](#🧭-Zona-horaria)
- [⚙ Procesos](#⚙-Procesos)

---
<br>

## 🖥 CPU

> Ver detalles de procesador(es)
```
$ cat /proc/cpuinfo
```

<br>

## 🧮 Memoria

>  Mostrar el uso de memoria RAM 

```
$ free -h
```
 - **-h** Leíble por humanos *(Kb, Mb, Gb ...)*

> Ver detalles de la memoria
```
$ cat /proc/meminfo
```

<br>

## 💽 Almacenamiento

> Mostrar el tamaño usado por directorios

```bash
$ du -s -h -c [path]
```
 - **-c** Muestra el peso total.
 - **-s** Sumariza los contenidos.
 - **-h** Leíble por humanos (tamaño).


> Muestra la utilización de los discos montados en el sistema.
```bash
$ df -h
```
 - **-h** Leíble por humanos (tamaño).


<br>

## 🧭 Zona horaria

> Mostrar la zona horaria configurada
```
$ timedatectl
```

> Mostrar las zonas disponibles
```
$ timedatectl list-timezones
```

> Cambiar la zona horaria
```
$ timedatectl set-timezone [tz]
```

<br>

## ⚙ Procesos

> Ver los procesos
```bash
ps
```

> Ver procesos en tiempo real
```bash
$ top
```

> Ver procesos en tiempo real (colores)
```bash
$ htop
```