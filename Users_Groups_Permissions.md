# Usuarios, grupos y permisos


**Tabla de contenidos**

  - [👤 Usuarios](#👤-Usuarios)
  - [👥 Grupos](#👥-Grupos)

---

<br>

## 👤 Usuarios

> Crear un usuario  


```bash
$ sudo adduser [username] # *Con contraseña y datos adicionales
$ sudo useradd [username] # *Sin contraseña ni datos adicionales
```

> Asignar una contraseña a un usuario   

```bash
$ sudo passwd [username]
```


> Eliminar un usuario.

```bash
$ sudo userdel [username]
```


<br>

## 👥 Grupos

> Listamos los grupos del usuario activo

```bash
$ groups
```

> Para crear/eliminar grupos

```bash
$ sudo groupadd [groupname]
$ sudo groupdel [groupname]
```


> Agregar usuario a un grupo
```bash
$ sudo adduser [username] [groupname]
$ sudo gpasswd -a [username] [groupname] 
$ sudo usermod -aG [groupname] [username]
```

> Eliminar usuario de un grupo
```bash
$ sudo gpasswd -d [username] [groupname]
$ sudo gpasswd --delete [username] [groupname] # toma efecto en el próximo login
$ sudo deluser [username] [groupname]
```